package opendata.incremental.metrics;

import org.spoofax.interpreter.terms.IStrategoTerm;
import org.spoofax.sunshine.statistics.Statistics;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

public class advance_stats_0_0 extends Strategy {
	public static advance_stats_0_0 instance = new advance_stats_0_0();

	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm current) {

		Statistics.toNext();
		
		return current;
	}

}
