package opendata.incremental.metrics;

import org.spoofax.interpreter.terms.IStrategoTerm;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

public class IndexUtils {

	public static Multiset<IStrategoTerm> diff(IStrategoTerm leftIndex,
			IStrategoTerm rightIndex) {
		return Multisets.difference(makeMultiset(leftIndex),
				makeMultiset(rightIndex));
	}

	public static Multiset<IStrategoTerm> makeMultiset(IStrategoTerm term) {
		Multiset<IStrategoTerm> multiset = HashMultiset.create();
		for (IStrategoTerm kid : term) {
			multiset.add(kid);
		}
		return multiset;
	}
}
