package opendata.incremental.metrics;

import org.spoofax.sunshine.statistics.BoxValidatable;
import org.spoofax.sunshine.statistics.Statistics;

public class StatsUtils {
	public static void stat(String label, int val) {
		Statistics.addDataPoint(label, new BoxValidatable<Integer>(val));
	}

	public static void stat(String label, long val) {
		Statistics.addDataPoint(label, new BoxValidatable<Long>(val));
	}

	public static void stat(String label, double val) {
		Statistics.addDataPoint(label, new BoxValidatable<Double>(val));
	}
}
