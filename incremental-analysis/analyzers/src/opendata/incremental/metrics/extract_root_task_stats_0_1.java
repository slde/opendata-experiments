package opendata.incremental.metrics;

import java.util.HashSet;

import org.spoofax.interpreter.terms.IStrategoTerm;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

public class extract_root_task_stats_0_1 extends Strategy {
	public static extract_root_task_stats_0_1 instance = new extract_root_task_stats_0_1();

	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm current,
			IStrategoTerm tasks) {
		HashSet<IStrategoTerm> taskIDs = new HashSet<IStrategoTerm>();
		for (IStrategoTerm task : tasks) {
			taskIDs.add(task.getSubterm(0));
		}
		for (IStrategoTerm task : tasks) {
			for (IStrategoTerm taskDep : task.getSubterm(2)) {
				taskIDs.remove(taskDep);
			}
		}

		StatsUtils.stat("ROOTASKS", taskIDs.size());

		return current;
	}
}
