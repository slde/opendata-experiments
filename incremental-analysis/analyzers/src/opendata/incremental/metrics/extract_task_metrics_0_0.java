package opendata.incremental.metrics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.spoofax.interpreter.terms.IStrategoAppl;
import org.spoofax.interpreter.terms.IStrategoInt;
import org.spoofax.interpreter.terms.IStrategoTerm;
import org.spoofax.interpreter.terms.IStrategoTuple;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

public class extract_task_metrics_0_0 extends Strategy {
	public static extract_task_metrics_0_0 instance = new extract_task_metrics_0_0();

	
	private Set<String> instrSet = new HashSet<String>();
	
	
	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm tasks) {

		// stats per global

		ArrayList<IStrategoTuple> taskList = new ArrayList<IStrategoTuple>();
		for (IStrategoTerm taskTrm : tasks) {
			taskList.add((IStrategoTuple) taskTrm);
		}
		doStats("GLOBAL", taskList);

		// stats per bin

		// create bins

		HashMap<String, Set<IStrategoTuple>> tasksByInstruction = new HashMap<String, Set<IStrategoTuple>>();

		for (IStrategoTerm task : tasks) {
			IStrategoTuple taskTup = (IStrategoTuple) task;
			String constr = ((IStrategoAppl) taskTup.getSubterm(1)).getName();
			instrSet.add(constr);
			Set<IStrategoTuple> taskForInstr = tasksByInstruction.get(constr);
			if (taskForInstr == null) {
				taskForInstr = new HashSet<IStrategoTuple>();
				tasksByInstruction.put(constr, taskForInstr);
			}
			taskForInstr.add(taskTup);
		}
		StatsUtils.stat("NUMINSTR", tasksByInstruction.size());
		for (String instr : instrSet) {
			Set<IStrategoTuple> byInstr = tasksByInstruction.get(instr);
			if(byInstr == null){
				byInstr = new HashSet<IStrategoTuple>();
			}
			doStats(instr, byInstr);
		}
		
//		for (Entry<String, Set<IStrategoTuple>> byInstr : tasksByInstruction
//				.entrySet()) {
//		}

		return tasks;
	}

	public static void doStats(String instr, Collection<IStrategoTuple> taskList) {
		int totalTasks = taskList.size();

		int totalExecutions = 0;
		long totalExecTime = 0;
		Collection<Double> perRepExecTime = new ArrayList<Double>(totalTasks);
		long totalDeps = 0;
		Collection<Long> numDependencies = new ArrayList<Long>(totalTasks);
		int evaledTasks = 0;
		for (IStrategoTuple task : taskList) {
			int repetitions = ((IStrategoInt) task.getSubterm(6)).intValue();
			if (repetitions >= 0){
				totalExecutions += repetitions;
			}
			long execTime = ((IStrategoInt) task.getSubterm(5)).intValue();
			totalExecTime += execTime;

			if (repetitions > 0) {
				perRepExecTime.add(new Long(execTime).doubleValue()
						/ repetitions);
				++evaledTasks;
			}
			int numDeps = task.getSubterm(2).getSubtermCount();
			numDependencies.add(new Long(numDeps));
			totalDeps += numDeps;
		}
		StatsUtils.stat("EVALDTASKS-" + instr, evaledTasks);
		StatsUtils.stat("NUM-" + instr, totalTasks);
		StatsUtils.stat("NUMEXECD-" + instr, totalExecutions);
		StatsUtils.stat("EXECTIME-" + instr + "-TOTAL", totalExecTime);
		boolean enableExecTime = perRepExecTime.size() > 0;
		StatsUtils.stat("EXECTIME-" + instr + "-MEAN",
				enableExecTime ? MathLib.meanDouble(perRepExecTime) : 0);
		StatsUtils.stat("EXECTIME-" + instr + "-MEDIAN",
				enableExecTime ? MathLib.medianDouble(perRepExecTime) : 0);
		StatsUtils.stat("EXECTIME-" + instr + "-SDEV",
				enableExecTime ? MathLib.sdevDouble(perRepExecTime) : 0);
		StatsUtils.stat("EXECTIME-" + instr + "-MIN",
				enableExecTime ? MathLib.minDouble(perRepExecTime) : 0);
		StatsUtils.stat("EXECTIME-" + instr + "-MAX",
				enableExecTime ? MathLib.maxDouble(perRepExecTime) : 0);
		StatsUtils.stat("DEPS-" + instr + "-TOTAL", totalDeps);
		boolean enableDeps = numDependencies.size() > 0;
		StatsUtils.stat("DEPS-" + instr + "-MEAN",
				enableDeps ? MathLib.mean(numDependencies) : 0);
		StatsUtils.stat("DEPS-" + instr + "-MEDIAN",
				enableDeps ? MathLib.median(numDependencies) : 0);
		StatsUtils.stat("DEPS-" + instr + "-SDEV",
				enableDeps ? MathLib.sdev(numDependencies) : 0);
		StatsUtils.stat("DEPS-" + instr + "-MIN",
				enableDeps ? MathLib.min(numDependencies) : 0);
		StatsUtils.stat("DEPS-" + instr + "-MAX",
				enableDeps ? MathLib.max(numDependencies) : 0);
	}

	

}
