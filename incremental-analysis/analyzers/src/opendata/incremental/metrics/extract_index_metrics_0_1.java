package opendata.incremental.metrics;

import java.util.ArrayList;
import java.util.Collection;

import org.spoofax.interpreter.terms.IStrategoAppl;
import org.spoofax.interpreter.terms.IStrategoString;
import org.spoofax.interpreter.terms.IStrategoTerm;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

public class extract_index_metrics_0_1 extends Strategy {
	public static extract_index_metrics_0_1 instance = new extract_index_metrics_0_1();

	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm index,
			IStrategoTerm label) {
		String statlabel = ((IStrategoString) label).stringValue();
		int defs = 0;
		int uses = 0;
		int props = 0;

		Collection<Long> uri_depths = new ArrayList<Long>(
				index.getSubtermCount());
		int totaldepth = 0;
		for (IStrategoTerm indexTrm : index) {
			String constr = ((IStrategoAppl) indexTrm).getName();
			if (constr.equals("Use")) {
				++uses;
			} else if (constr.equals("Def")) {
				++defs;
			} else if (constr.equals("Prop")) {
				++props;
			}
			if (constr.equals("Def") || constr.equals("Prop")) {
				int depth = indexTrm.getSubterm(0).getSubterm(1)
						.getSubtermCount();
				uri_depths.add(new Long(depth));
				totaldepth += depth;
			}
		}

		StatsUtils.stat("NUMDEFS-" + statlabel, defs);
		StatsUtils.stat("NUMUSES-" + statlabel, uses);
		StatsUtils.stat("NUMPROPS-" + statlabel, props);
		StatsUtils.stat("URIDEPTH-" + statlabel + "-TOTAL", totaldepth);
		boolean enableDepths = uri_depths.size() > 0;
		StatsUtils.stat("URIDEPTH-" + statlabel + "-MEAN",
				enableDepths ? MathLib.mean(uri_depths) : 0);
		StatsUtils.stat("URIDEPTH-" + statlabel + "-MEDIAN",
				enableDepths ? MathLib.median(uri_depths) : 0);
		StatsUtils.stat("URIDEPTH-" + statlabel + "-SDEV",
				enableDepths ? MathLib.sdev(uri_depths) : 0);
		StatsUtils.stat("URIDEPTH-" + statlabel + "-MIN",
				enableDepths ? MathLib.min(uri_depths) : 0);
		StatsUtils.stat("URIDEPTH-" + statlabel + "-MAX",
				enableDepths ? MathLib.max(uri_depths) : 0);

		return index;
	}
}
