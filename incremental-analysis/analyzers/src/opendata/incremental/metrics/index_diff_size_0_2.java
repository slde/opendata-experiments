package opendata.incremental.metrics;

import org.spoofax.interpreter.terms.IStrategoTerm;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

public class index_diff_size_0_2 extends Strategy {
	public static index_diff_size_0_2 instance = new index_diff_size_0_2();

	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm current,
			IStrategoTerm leftIndex, IStrategoTerm rightIndex) {
		return context.getFactory().makeInt(
				IndexUtils.diff(leftIndex, rightIndex).size());
	}

}
