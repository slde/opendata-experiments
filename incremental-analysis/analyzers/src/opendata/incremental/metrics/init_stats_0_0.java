package opendata.incremental.metrics;

import org.spoofax.interpreter.terms.IStrategoString;
import org.spoofax.interpreter.terms.IStrategoTerm;
import org.spoofax.sunshine.Environment;
import org.spoofax.sunshine.drivers.SunshineMainArguments;
import org.spoofax.terms.StrategoString;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

public class init_stats_0_0 extends Strategy {
	public static init_stats_0_0 instance = new init_stats_0_0();

	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm targetFile) {
		if (!(targetFile instanceof StrategoString))
			return null;
		Environment.INSTANCE().setMainArguments(new SunshineMainArguments());
		Environment.INSTANCE().getMainArguments().statstarget = ((IStrategoString) targetFile)
				.stringValue();

		return targetFile;
	}

}
