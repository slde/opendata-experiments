package opendata.incremental.metrics;

import org.spoofax.interpreter.terms.IStrategoInt;
import org.spoofax.interpreter.terms.IStrategoString;
import org.spoofax.interpreter.terms.IStrategoTerm;
import org.spoofax.sunshine.statistics.BoxValidatable;
import org.spoofax.sunshine.statistics.IValidatable;
import org.spoofax.sunshine.statistics.Statistics;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

public class put_stat_0_1 extends Strategy {
	public static put_stat_0_1 instance = new put_stat_0_1();

	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm value,
			IStrategoTerm label) {
		if (!(label instanceof IStrategoString))
			return null;

		IValidatable<?> box = null;
		if (value instanceof IStrategoString) {
			box = new BoxValidatable<String>(
					((IStrategoString) value).stringValue());
		} else if (value instanceof IStrategoInt) {
			box = new BoxValidatable<Integer>(((IStrategoInt) value).intValue());
		} else {
			return null;
		}

		Statistics.addDataPoint(((IStrategoString) label).stringValue(), box);

		return value;
	}

}
