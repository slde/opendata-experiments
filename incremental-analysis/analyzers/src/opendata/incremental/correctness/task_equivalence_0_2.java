package opendata.incremental.correctness;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.spoofax.interpreter.terms.IStrategoTerm;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

import com.google.common.collect.Sets;

public class task_equivalence_0_2 extends Strategy {
	public static task_equivalence_0_2 instance = new task_equivalence_0_2();

	@Override
	public IStrategoTerm
		invoke(Context context, IStrategoTerm current, IStrategoTerm fullTasks, IStrategoTerm incrTasks) {
		final Set<IStrategoTerm> fullSet = makeSet(fullTasks);
		final Set<IStrategoTerm> incrSet = makeSet(incrTasks);

		// Compare if incremental and full task engine contain exactly the same task identifiers.
		final Set<IStrategoTerm> setDiff = Sets.symmetricDifference(fullSet, incrSet);
		if(setDiff.size() > 0) {
			System.err.println("Task engines are not equivalent: ");
			for(IStrategoTerm diff : setDiff) {
				if(fullSet.contains(diff)) {
					System.err.println("Only in full: " + diff);
				} else {
					System.err.println("Only in incr: " + diff);
				}
			}
			return null;
		}

		// Compare if each incremental task has the same identifier, instruction, result and messages in the full tasks.
		// Only need to check one way since previous comparison already guarantees that both sets are the same.
		final Map<IStrategoTerm, IStrategoTerm> fullMap = makeMap(fullTasks);
		final Map<IStrategoTerm, IStrategoTerm> incrMap = makeMap(incrTasks);
		for(IStrategoTerm incrTask : incrMap.values()) {
			final IStrategoTerm incrTaskID = incrTask.getSubterm(0);
			final IStrategoTerm incrInstr = incrTask.getSubterm(1);
			final IStrategoTerm incrResults = incrTask.getSubterm(3);
			final IStrategoTerm incrMessages = incrTask.getSubterm(4);

			final IStrategoTerm fullTask = fullMap.get(incrTaskID);
			final IStrategoTerm fullTaskID = fullTask.getSubterm(0);
			final IStrategoTerm fullInstr = fullTask.getSubterm(1);
			final IStrategoTerm fullResults = fullTask.getSubterm(3);
			final IStrategoTerm fullMessages = incrTask.getSubterm(4);
			
			if(!incrTaskID.equals(fullTaskID)) {
				System.err.println("Mismatching task identifier. full: " + fullTaskID + " incr: " + incrTaskID);
				return null;
			}
			if(!incrInstr.equals(fullInstr)) {
				System.err.println("Mismatching instruction. full: " + fullInstr + " incr: " + incrInstr);
				return null;
			}
			if(!incrResults.equals(fullResults)) {
				System.err.println("Mismatching results. full: " + fullResults + " incr: " + incrResults);
				return null;
			}
			if(!incrMessages.equals(fullMessages)) {
				System.err.println("Mismatching messages. full: " + fullMessages + " incr: " + incrMessages);
				return null;
			}
		}

		return current;
	}

	private Map<IStrategoTerm, IStrategoTerm> makeMap(IStrategoTerm term) {
		Map<IStrategoTerm, IStrategoTerm> map = new HashMap<IStrategoTerm, IStrategoTerm>();
		for(IStrategoTerm kid : term) {
			map.put(kid.getSubterm(0), kid);
		}
		return map;
	}

	private Set<IStrategoTerm> makeSet(IStrategoTerm term) {
		Set<IStrategoTerm> set = new HashSet<IStrategoTerm>();
		for(IStrategoTerm kid : term) {
			set.add(kid.getSubterm(0));
		}
		return set;
	}
}
