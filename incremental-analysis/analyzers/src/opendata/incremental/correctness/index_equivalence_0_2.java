package opendata.incremental.correctness;

import opendata.incremental.metrics.IndexUtils;

import org.spoofax.interpreter.terms.IStrategoTerm;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;

import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

public class index_equivalence_0_2 extends Strategy {
	public static index_equivalence_0_2 instance = new index_equivalence_0_2();

	@Override
	public IStrategoTerm
		invoke(Context context, IStrategoTerm current, IStrategoTerm fullIndex, IStrategoTerm incrIndex) {
		final Multiset<IStrategoTerm> fullSet = IndexUtils.makeMultiset(fullIndex);
		final Multiset<IStrategoTerm> incrSet = IndexUtils.makeMultiset(incrIndex);

		// Takes the difference between incremental and full indexes.
		final Multiset<IStrategoTerm> fullMinusIncrDiff = Multisets.difference(incrSet, fullSet);
		if(fullMinusIncrDiff.size() > 0) {
			System.err.println("Indexes are not equivalent (full - diff): ");
			for(IStrategoTerm diff : fullMinusIncrDiff.elementSet()) {
				System.err.println(diff + " x " + incrSet.count(diff) + ". full: " + fullSet.count(diff) + " incr: "
					+ incrSet.count(diff));
			}
			return null;
		}

		// Takes the difference between full and incremental indexes.
		final Multiset<IStrategoTerm> incrMinusFullDiff = Multisets.difference(fullSet, incrSet);
		if(incrMinusFullDiff.size() > 0) {
			System.err.println("Indexes are not equivalent (diff - full): ");
			for(IStrategoTerm diff : incrMinusFullDiff.elementSet()) {
				System.err.println(diff + " x " + incrSet.count(diff) + ". full: " + fullSet.count(diff) + " incr: "
					+ incrSet.count(diff));
			}
			return null;
		}

		return current;
	}

}
