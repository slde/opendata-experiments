#! /bin/bash
ant clean &&
ant -lib lib/ &&
java -Xmx4g -Xms4g -cp lib/strategoxt.jar:lib/guava.jar:lib/task.jar:lib/sunshine_gitdrive.jar run metrics.jar main-metrics 1 346 ../results/yellowgrass/incremental/ &&
java -Xmx4g -Xms4g -cp lib/strategoxt.jar:lib/guava.jar:lib/task.jar:lib/sunshine_gitdrive.jar run metrics.jar main-metrics 1 346 ../results/yellowgrass/non-incremental/ &&
java -Xmx4g -Xms4g -cp lib/strategoxt.jar:lib/guava.jar:lib/task.jar:lib/sunshine_gitdrive.jar run metrics.jar main-metrics 1 64 ../results/blog/incremental/ &&
java -Xmx4g -Xms4g -cp lib/strategoxt.jar:lib/guava.jar:lib/task.jar:lib/sunshine_gitdrive.jar run metrics.jar main-metrics 1 64 ../results/blog/non-incremental/