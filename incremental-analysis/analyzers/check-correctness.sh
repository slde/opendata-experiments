#! /bin/bash
ant clean
ant -lib lib &&
java -cp lib/strategoxt.jar:lib/guava.jar:lib/task.jar:lib/sunshine_gitdrive.jar run correctness.jar main-correctness 1 215 ../results/yellowgrass/non-incremental/ ../results/yellowgrass/incremental/ &&
java -cp lib/strategoxt.jar:lib/guava.jar:lib/task.jar:lib/sunshine_gitdrive.jar run correctness.jar main-correctness 1 64 ../results/blog/non-incremental/ ../results/blog/incremental/
