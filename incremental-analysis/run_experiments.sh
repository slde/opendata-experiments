#! /bin/bash


SUNSHINE="java -ea:org.spoofax.sunshine... -server -Xss16m -Xms4G -Xmx4G -cp binaries/sunshine/terms_custom.jar:binaries/sunshine/sunshine_gitdrive.jar org.spoofax.sunshine.gitdrive.Main"

WEBDSL="binaries/webdsl2"

SUNSHINE_WEBDSL="
$SUNSHINE \
	--lang WebDSL \
	--jar $WEBDSL/include/webdsl.jar \
	--jar $WEBDSL/include/webdsl-java.jar \
	--jar $WEBDSL/lib/task.jar \
	--table $WEBDSL/include/WebDSL.tbl \
	--ssymb Unit \
	--ext app
	--warmups 3"

# Run YellowGrass

cd subjects
rm -rf yellowgrass
tar -xf yellowgrass.git.tgz
cd ..

### INCREMENTAL ###

mkdir -p results/yellowgrass/incremental

$SUNSHINE_WEBDSL \
	--project subjects/yellowgrass \
	--with-lib $WEBDSL/client-lib \
	--builder webdsl-metrics \
	--build-on yellowgrass.app \
	--stats results/yellowgrass/incremental/stats.csv 2>&1 || exit 1

### FULL ###

mkdir -p results/yellowgrass/non-incremental

$SUNSHINE_WEBDSL \
	--project subjects/yellowgrass \
	--with-lib $WEBDSL/client-lib \
	--builder webdsl-metrics \
	--build-on yellowgrass.app \
	--non-incremental \
	--stats results/yellowgrass/non-incremental/stats.csv 2>&1 || exit 1




# Run Blog

cd subjects
rm -rf blog
tar -xf blog.git.tgz
cd ..

### INCREMENTAL ###

mkdir -p results/blog/incremental

$SUNSHINE_WEBDSL \
	--project subjects/blog \
	--with-lib $WEBDSL/client-lib \
	--builder webdsl-metrics \
	--build-on blog.app \
	--stats results/blog/incremental/stats.csv 2>&1 || exit 1

### FULL ###

mkdir -p results/blog/non-incremental

$SUNSHINE_WEBDSL \
	--project subjects/blog \
	--with-lib $WEBDSL/client-lib \
	--builder webdsl-metrics \
	--build-on blog.app \
	--non-incremental \
	--stats results/blog/non-incremental/stats.csv 2>&1 || exit 1

find results -name fsmonitor.bin -exec rm {} \;


