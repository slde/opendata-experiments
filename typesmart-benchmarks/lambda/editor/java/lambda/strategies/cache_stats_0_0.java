package lambda.strategies;

import org.spoofax.interpreter.terms.IStrategoTerm;
import org.strategoxt.lang.Context;
import org.strategoxt.lang.Strategy;
import org.strategoxt.lang.typesmart.TypesmartSyntaxTermFactory;
import org.strategoxt.lang.typesmart.TypesmartTermFactory;

/**
 * Example Java strategy implementation.
 * 
 * This strategy can be used by editor services and can be called in Stratego
 * modules by declaring it as an external strategy as follows:
 * 
 * <code>
 *  external java-strategy(|)
 * </code>
 * 
 * @see InteropRegisterer This class registers java_strategy_0_0 for use.
 */
public class cache_stats_0_0 extends Strategy {

	public static cache_stats_0_0 instance = new cache_stats_0_0();

	@Override
	public IStrategoTerm invoke(Context context, IStrategoTerm current) {
		TypesmartSyntaxTermFactory factory = (TypesmartSyntaxTermFactory) TypesmartSyntaxTermFactory
				.getTypesmartInstance(context.getFactory());
		int hits = factory.cacheHits;
		int misses = factory.cacheMisses;
		return factory
				.makeTuple(factory.makeInt(hits), factory.makeInt(misses));
	}

}
